using System;
using System.Linq;
using TheAM.Framework.BusinessLayer.DataContext;
using TheAM.Modules.UserData.Entities;

namespace TheAM.Facade.Migrations
{
    public static class DbInitializer
    {

        public static void Initialize(ApiDbContext context)
        {
            if (context.Users.Any())
                return;

            
            context.Users.Add(new User()
            {
                Id = new Guid("f32eab73-bd85-487b-8e74-4f0f503ff613"),
                ParentUserId = Guid.Empty,
                UserName = "Admin",
                Email = "info@carlosluis.es",
                ApiKey =
                    "5DB3BE4022936DA7DF5FA06D6751E3C2A51451D78CC2589E90A615F0CA625B9B952A59A0C521446723833C2E87E1F55C21FEB2AA026195C7A75DE17B3A7D50B9",
                IsAdmin = true
            });
            context.SaveChanges();
        }
    }
}