using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TheAM.Framework.BusinessLayer.DataContext;
using TheAM.Framework.BusinessLayer.Interfaces;
using TheAM.Framework.BusinessLayer.Security;
using TheAM.Framework.SharedLibs.Extensions;
using TheAM.Modules.CustomerCore;
using TheAM.Modules.UserCore;

namespace TheAM.Facade.Middleware
{
    public static class ServicesExtensions
    {
        public static IServiceCollection AddDependency(this IServiceCollection services)
        {
            services.AddTransient<ICustomerService, CustomerService>();
            services.AddTransient<IUserService, UserService>();
            services.AddScoped<ISecurityHandler, SecurityHandler>();

            return services;
        }
        
        public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration config)
        {
            var dbEngine = config.GetValue<string>("DB_Engine");
            if (dbEngine.EqualsIgnoringCase("mssql"))
            {
                var connectionString = config.GetConnectionString("SQLServerConnectionString")
                    .Replace("{{DB_ENDPOINT}}", config.GetValue<string>("DB_ENDPOINT"))
                    .Replace("{{DB_PORT}}", config.GetValue<string>("DB_PORT"))
                    .Replace("{{DB_PASSWORD}}", config.GetValue<string>("DB_PASSWORD"))
                    .Replace("{{DB_USERNAME}}", config.GetValue<string>("DB_USERNAME"))
                    .Replace("{{DB_NAME}}", config.GetValue<string>("DB_NAME"));
                services.AddDbContext<ApiDbContext>(opt => opt.UseSqlServer(connectionString,
                    b => b.MigrationsAssembly("TheAM.Facade")));
            }
            else
                services.AddDbContext<ApiDbContext>(opt => opt.UseInMemoryDatabase("CRMStorage"));

            return services;
        }
    }
}