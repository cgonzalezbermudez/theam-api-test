using System;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using TheAM.Facade.Middleware;
using TheAM.Facade.Migrations;
using TheAM.Framework.BusinessLayer.DataContext;

namespace TheAM.Facade
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "API Test", Description = "The Agile Monkeys -  The CRM Service", Version = "v1",
                        Contact = new OpenApiContact
                        {
                            Name = "Carlos L. Gonzalez", 
                            Url = new Uri("https://www.linkedin.com/in/cgonzalezbermudez/")
                        }
                    });
            });
            services.AddDatabase(Configuration);
            services.AddDependency();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            UpdateDatabase(app);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TheAM.Facade v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        public void UpdateDatabase(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            using var context = serviceScope.ServiceProvider.GetService<ApiDbContext>();
            if (context == null)
                return;

            if (Configuration.GetValue<bool>("DB_MIGRATE") && context.Database.IsRelational())
                context.Database.Migrate();

            if (Configuration.GetValue<bool>("DB_INITIALIZE"))
                DbInitializer.Initialize(context);
        }
    }
}