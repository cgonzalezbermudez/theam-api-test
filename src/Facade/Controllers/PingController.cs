﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace TheAM.Facade.Controllers
{
    [ApiController]
    [Route("api")]
    public class PingController : ControllerBase
    {
        private readonly ILogger<PingController> _logger;

        public PingController(ILogger<PingController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public string Get() => $"Service up";
    }
}
