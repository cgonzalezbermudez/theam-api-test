using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace TheAM.Framework.DataLayer.Entities
{
    [DataContract]

    public abstract class Entity : IEntity
    {
        private PropertyInfo[] _propertyInfos = null;
        private Guid? _id;

        protected Entity() => _id = Guid.NewGuid();

        public virtual Guid Id
        {
            get => _id ?? Guid.NewGuid();
            set => _id = value;
        }

        public virtual bool Equals(IEntity other)
        {
            if (ReferenceEquals(null, other)) return false;
            return ReferenceEquals(this, other) || other.Id.Equals(Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (!(obj is IEntity)) return false;
            return Equals((IEntity) obj);
        }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            _propertyInfos ??= GetType().GetProperties();

            var sb = new StringBuilder();
            _propertyInfos.ToList().ForEach(prop =>
                sb.AppendLine($"{prop.Name}: {(prop.GetValue(this, null) ?? "(null)")}"));

            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson() => JsonConvert.SerializeObject(this, Formatting.Indented);
        
        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            _propertyInfos ??= GetType().GetProperties();
            var hashCode = 41;
            _propertyInfos.ToList().ForEach(prop =>
            {
                var value = prop.GetValue(this, null);
                if (value != null)
                {
                    hashCode = hashCode * 59 + value.GetHashCode();
                }
            });
            return hashCode;
        }

        public virtual object Clone() => (Entity) MemberwiseClone();

        public static bool operator ==(Entity left, IEntity right) => Equals(left, right);

        public static bool operator !=(Entity left, IEntity right) => !Equals(left, right);
    }
}