using System;

namespace TheAM.Framework.DataLayer.Entities
{
    public interface IEntity : IEquatable<IEntity>, ICloneable
    {
        Guid Id { get; set; }
    }
}