﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TheAM.Framework.BusinessLayer.Filters;

namespace TheAM.Framework.BusinessLayer.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ApiKeyAuthorizeAttribute : TypeFilterAttribute
    {
        public ApiKeyAuthorizeAttribute() : base(typeof(ApiKeyAuthorizeAsyncFilter))
        {
        }
    }

    // {
    //     private const string Apikeyname = "X-API-KEY";
    //
    //
    //     public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    //     {
    //         if (!context.HttpContext.Request.Headers.TryGetValue(Apikeyname, out _))
    //         {
    //             context.Result = new ContentResult()
    //             {
    //                 StatusCode = 401,
    //                 Content = "Api Key was not provided"
    //             };
    //             return;
    //         }
    //         
    //         var user = 
    //         
    //         // //TODO: Check ApiKey from Database.
    //         //
    //         // var appSettings = context.HttpContext.RequestServices.GetRequiredService<IConfiguration>();
    //         //
    //         // var apiKey = appSettings.GetValue<string>(Apikeyname);
    //         //
    //         // if (!apiKey.Equals(extractedApiKey))
    //         // {
    //         //     context.Result = new ContentResult()
    //         //     {
    //         //         StatusCode = 401,
    //         //         Content = "Api Key is not valid"
    //         //     };
    //         //     return;
    //         // }
    //         //
    //         await next();
        // }
    // }
}
