using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using TheAM.Framework.BusinessLayer.Interfaces;
using TheAM.Framework.SharedLibs.Extensions;
using TheAM.Modules.UserData.Entities;

namespace TheAM.Framework.BusinessLayer.Security
{
    public class SecurityHandler : ISecurityHandler
    {
        private readonly ILogger<SecurityHandler> _logger;
        private readonly IUserService _userService;

        public SecurityHandler(ILogger<SecurityHandler> logger, IUserService userService)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public Task<User> ResolveCurrentUser(HttpContext httpContext)
        {
            var id = httpContext.User.Identities.First(c => c.AuthenticationType == "ApiKey").Claims
                .First(c => c.Type == "userId")
                .Value;
            return _userService.Get(id.IsNullOrWhiteSpace()?Guid.Empty:new Guid(id));
        }
    }
}