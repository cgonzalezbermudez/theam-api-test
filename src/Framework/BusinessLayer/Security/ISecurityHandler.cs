using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using TheAM.Modules.UserData.Entities;

namespace TheAM.Framework.BusinessLayer.Security
{
    public interface ISecurityHandler
    {
        Task<User> ResolveCurrentUser(HttpContext httpContext);
    }
}