using Microsoft.EntityFrameworkCore;
using TheAM.Modules.CustomerData.Entities;
using TheAM.Modules.UserData.Entities;

namespace TheAM.Framework.BusinessLayer.DataContext
{
    public class ApiDbContext : DbContext
    {
        public ApiDbContext()
        {
        }

        public ApiDbContext(DbContextOptions<ApiDbContext> options)
            : base(options)
        {
        }
        
        public DbSet<Customer> Customers { get; set; }
        public DbSet<User> Users { get; set; }
    }
}