using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using TheAM.Framework.BusinessLayer.Interfaces;

namespace TheAM.Framework.BusinessLayer.Filters
{
    public class ApiKeyAuthorizeAsyncFilter : IAsyncAuthorizationFilter
    {
        public static string ApiKeyHeaderName = "ApiKey";
        public static string UserNameHeaderName = "UserName";

        private readonly ILogger<ApiKeyAuthorizeAsyncFilter> _logger;
        private readonly IUserService _userService;

        public ApiKeyAuthorizeAsyncFilter(ILogger<ApiKeyAuthorizeAsyncFilter> logger, IUserService userService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var request = context.HttpContext.Request;
            var isApiKeyPresent = context.HttpContext.Request.Headers.TryGetValue(ApiKeyHeaderName, out var apiKeyValue);

            if (isApiKeyPresent)
            {
                _logger.LogDebug("Found the header {ApiKeyHeader}. Starting API Key validation", ApiKeyHeaderName);
                if (apiKeyValue.Count != 0 && !string.IsNullOrWhiteSpace(apiKeyValue))
                {
                    if (context.HttpContext.Request.Headers.TryGetValue(UserNameHeaderName, out var userName) &&
                        userName.Count != 0 && !string.IsNullOrWhiteSpace(userName))
                    {
                        var user = await _userService.IsAuthorized(userName, apiKeyValue);
                        if (user != null)
                        {
                            _logger.LogDebug("User {UserName} successfully logged in with key {ApiKey}",
                                user.UserName, user.ApiKey);
                            var apiKeyClaim = new Claim("apikey", user.ApiKey);
                            var subject = new Claim(ClaimTypes.Name, user.UserName);
                            var userIdClaim = new Claim("userId", user.Id.ToString());
                            var principal =
                                new ClaimsPrincipal(
                                    new ClaimsIdentity(new List<Claim> {apiKeyClaim, subject, userIdClaim}, "ApiKey"));
                            context.HttpContext.User = principal;

                            return;
                        }

                        _logger.LogWarning("User {UserName} with ApiKey {ApiKey} is not authorized", userName,
                            apiKeyValue);
                    }
                    else
                    {
                        _logger.LogWarning("{HeaderName} header not found or it was null or empty", UserNameHeaderName);
                    }
                }
                else
                {
                    _logger.LogWarning("{HeaderName} header found, but api key was null or empty", ApiKeyHeaderName);
                }
            }
            else
            {
                _logger.LogWarning("No ApiKey header found");
            }

            context.Result = new UnauthorizedResult();
        }
    }
}