using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TheAM.Framework.BusinessLayer.Security;
using TheAM.Modules.UserData.Entities;

namespace TheAM.Framework.BusinessLayer.Controllers
{
    public abstract class BasicController : ControllerBase
    {
        protected readonly ISecurityHandler SecurityHandler;

        protected BasicController(ISecurityHandler securityHandler)
        {
            SecurityHandler = securityHandler ?? throw new ArgumentNullException(nameof(securityHandler));
        }

        protected async Task<User> ResolveCurrentUserAsync() => await SecurityHandler.ResolveCurrentUser(HttpContext);
    }
}