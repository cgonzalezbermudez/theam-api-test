using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheAM.Modules.UserData.Entities;

namespace TheAM.Framework.BusinessLayer.Interfaces
{
    public interface IUserService
    {
        Task<int> Delete(User entity);
        Task<User> Put(User entity);
        Task<User> Post(User entity);
        Task<User> IsAuthorized(string userName, string apiKey);
        Task<User> Get(Guid entityId);
        Task<List<User>> Get();
    }
}