using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheAM.Modules.CustomerData.Entities;

namespace TheAM.Framework.BusinessLayer.Interfaces
{
    public interface ICustomerService
    {
        Task<int> Delete(Customer entity);
        Task<Customer> Put(Customer entity);
        Task<Customer> Post(Customer entity);
        Task<Customer> Get(Guid entityId);
        Task<List<Customer>> Get();
    }
}