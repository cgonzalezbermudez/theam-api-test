using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using TheAM.Framework.SharedLibs.Extensions;

namespace TheAM.Framework.SharedLibs.Converters
{
    public class DecimalFormatConverter : JsonConverter<decimal>
    {
        public override decimal Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) =>
            reader.GetDecimalSafe();

        public override void Write(Utf8JsonWriter writer, decimal value, JsonSerializerOptions options) => 
            writer.WriteStringValue($"{value:F2}");
    }
}