using System;
using System.Text.Json;
using static System.Decimal;

namespace TheAM.Framework.SharedLibs.Extensions
{
    public static class Utf8JsonReaderExtensions
    {
        public static decimal GetDecimalSafe(this Utf8JsonReader reader)
        {
            try
            {
                if (reader.TryGetDecimal(out var result1St))
                    return result1St;
            }
            catch (Exception) 
            {
                // ignored
                // Reader could not access to the value with a tryParse as decimal
            }

            return TryParse(reader.GetString(), out var result2Nd) ? result2Nd : Zero;
        }
    }
}