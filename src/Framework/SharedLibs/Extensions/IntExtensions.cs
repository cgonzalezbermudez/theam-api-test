﻿using System;

namespace TheAM.Framework.SharedLibs.Extensions
{
    public static class IntExtensions
    {
        public static bool IsGreatherThanZero(this int val) => val > 0;
        public static bool IsGreatherThanZero(this float val) => val > 0;
        public static bool IsGreatherThanZero(this long val) => val > 0;
        public static bool IsGreatherThanZero(this decimal val) => val > 0;
        public static bool IsGreatherThanZero(this decimal? val) => val > 0;
        public static bool IsGreatherThanZero(this short val) => val > 0;
        public static bool IsGreatherThanZero(this uint val) => val > 0;
        public static bool IsGreatherThanZero(this ulong val) => val > 0;
        public static bool IsGreatherThanZero(this ushort val) => val > 0;
        public static int UMinValue => Convert.ToInt32(uint.MinValue);
    }
}
