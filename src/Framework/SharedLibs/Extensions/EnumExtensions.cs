﻿using System;

namespace TheAM.Framework.SharedLibs.Extensions
{
    public static class EnumExtensions
    {
        public static int ToInt(this Enum enumeration) => Convert.ToInt32(enumeration);
        public static string ToIntString(this Enum enumeration) => ToInt(enumeration).ToString();
    }
}
