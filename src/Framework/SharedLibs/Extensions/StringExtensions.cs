﻿using System;

namespace TheAM.Framework.SharedLibs.Extensions
{
    public static class StringExtensions
    {
        public static bool EqualsIgnoringCase(this string str, string other)
        {
            if (!str.IsNullOrWhiteSpace())
            {
                return str.Equals(other, StringComparison.OrdinalIgnoreCase);
            }

            return other.IsNullOrWhiteSpace();
        }

        public static bool ContainsIgnoringCase(this string str, string content)
        {
            if (!str.IsNullOrWhiteSpace() && !content.IsNullOrWhiteSpace())
            {
                return str.IndexOf(content, StringComparison.OrdinalIgnoreCase) >= 0;
            }

            return false;
        }

        public static bool IsNullOrWhiteSpace(this string val) => string.IsNullOrWhiteSpace(val);

        public static T ToEnum<T>(this string enumValue) => (T)Enum.Parse(typeof(T), enumValue);
    }
}
