﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using TheAM.Framework.DataLayer.Entities;

namespace TheAM.Modules.UserData.Entities
{
    [DataContract]
    public class User : Entity
    {
        /// <summary>Gets or Sets the User Name</summary>
        [Key, DataMember(Name = "UserName", EmitDefaultValue = false), Required]
        public string UserName { get; set; }
        /// <summary>Gets or Sets the Email</summary>
        [DataMember(Name = "Email", EmitDefaultValue = false), Required]
        public string Email { get; set; }
        /// <summary>Gets or Sets the Api Key</summary>
        [DataMember(Name = "ApiKey", EmitDefaultValue = false), Required]
        public string ApiKey { get; set; }
        /// <summary>Gets or Sets Is Admin flag</summary>
        [DataMember(Name = "IsAdmin"), DefaultValue(false)]
        public bool IsAdmin { get; set; }
        /// <summary>Gets or Sets the Parent User Id</summary>
        public Guid ParentUserId { get; set; }
    }
}