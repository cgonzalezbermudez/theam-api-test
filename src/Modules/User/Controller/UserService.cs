using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TheAM.Framework.BusinessLayer.DataContext;
using TheAM.Framework.BusinessLayer.Interfaces;
using TheAM.Framework.SharedLibs.Extensions;
using TheAM.Modules.UserData.Entities;

namespace TheAM.Modules.UserCore
{
   public class UserService : IUserService
    {
        private readonly ApiDbContext _dbContext;

        public UserService(ApiDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<int> Delete(User entity)
        {
            var responseUser = _dbContext.Users.Remove(entity);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<User> Put(User entity)
        {
            var responseUser = _dbContext.Users.Update(entity);
            await _dbContext.SaveChangesAsync();
            return responseUser.Entity;
        }

        public async Task<User> Post(User entity)
        {
            var responseUser = await _dbContext.Users.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
            return responseUser.Entity;
        }

        public async Task<User> IsAuthorized(string userName, string apiKey) =>
            await _dbContext.Users.Where(u => u.ApiKey == apiKey && u.UserName == userName).FirstOrDefaultAsync();
        public async Task<User> Get(Guid entityId) => await _dbContext.Users.Where(u => u.Id == entityId).FirstOrDefaultAsync();
        public async Task<List<User>> Get() => await _dbContext.Users.ToListAsync();
    }
}