﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TheAM.Framework.BusinessLayer.Attributes;
using TheAM.Framework.BusinessLayer.Controllers;
using TheAM.Framework.BusinessLayer.Interfaces;
using TheAM.Framework.BusinessLayer.Security;
using TheAM.Framework.SharedLibs.Extensions;
using TheAM.Modules.UserData.Entities;

namespace TheAM.Modules.UserCore
{
    [Produces("application/json")]
    [ApiKeyAuthorize, ApiController, Route("api/[controller]")]
    public class UserController: BasicController
    {

        private readonly IUserService _userService;

        public UserController(IUserService userService, ISecurityHandler securityHandler) : base(securityHandler)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<IActionResult> Post([FromBody] User request)
        {
            var currentUser = await ResolveCurrentUserAsync();
            if (!currentUser.IsAdmin)
                return Unauthorized(currentUser);
            if (request.ParentUserId == Guid.Empty)
                request.ParentUserId = currentUser.Id;
            return Created("User", await _userService.Post(request));
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        public async Task<IActionResult> Put([FromBody] User request)
        {
            var currentUser = await ResolveCurrentUserAsync();
            if (!currentUser.IsAdmin)
                return Unauthorized(currentUser);
            return Accepted(await _userService.Put(request));
        }

        [HttpDelete("{userId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Delete([FromRoute] string userId)
        {
            var entity = await  _userService.Get(new Guid(userId));
            if (entity.ParentUserId == Guid.Empty)
                return Unauthorized(userId);
            var currentUser = await ResolveCurrentUserAsync();
            if (!currentUser.IsAdmin)
                return Unauthorized(currentUser);
            if (entity.ParentUserId != currentUser.Id || currentUser.ParentUserId != Guid.Empty)
                return Unauthorized(currentUser);
            return Ok(await _userService.Delete(entity));
        }

        [HttpGet("{userId?}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Get([FromRoute] string userId = "")
        {
            var currentUser = await ResolveCurrentUserAsync();
            if (currentUser.IsAdmin)
                return Ok(userId.IsNullOrWhiteSpace()? await _userService.Get() : await _userService.Get(new Guid(userId)));
            return Ok(currentUser);
        }
    }
}