﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using TheAM.Framework.DataLayer.Entities;

namespace TheAM.Modules.CustomerData.Entities
{
    [DataContract]
    public class Customer : Entity
    {
        /// <summary>Gets or Sets customer data - Name</summary>
        [Key, DataMember(Name = "Name", EmitDefaultValue = false)]
        public string Name { get; set; }
        /// <summary>Gets or Sets customer data - Surname</summary>
        [DataMember(Name = "Surname", EmitDefaultValue = false)]
        public string Surname { get; set; }
        /// <summary>Gets or Sets customer data - Photo source</summary>
        [DataMember(Name = "PhotoSrc", EmitDefaultValue = false)]
        public string PhotoSrc { get; set; }
        /// <summary>Gets or Sets customer data - User who created the customer</summary>
        public Guid CreatedBy { get; set; }
        /// <summary>Gets or Sets customer data - Latest user who updated the customer</summary>
        public Guid ModifiedBy { get; set; }
    }
}