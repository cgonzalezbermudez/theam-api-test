﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TheAM.Framework.BusinessLayer.Attributes;
using TheAM.Framework.BusinessLayer.Controllers;
using TheAM.Framework.BusinessLayer.Interfaces;
using TheAM.Framework.BusinessLayer.Security;
using TheAM.Framework.SharedLibs.Extensions;
using TheAM.Modules.CustomerData.Entities;

namespace TheAM.Modules.CustomerCore
{
    [Produces("application/json")]
    [ApiKeyAuthorize, ApiController, Route("api/[controller]")]
    public class CustomerController : BasicController
    {
        private readonly ICustomerService _customerService;
        public CustomerController(ICustomerService customerService, ISecurityHandler securityHandler) : base(securityHandler)
        {
            _customerService = customerService ?? throw new ArgumentNullException(nameof(customerService));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Customer request)
        {
            var currentUser = await ResolveCurrentUserAsync();
            request.CreatedBy = currentUser.Id;
            return Ok(await _customerService.Post(request));
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] Customer request)
        {
            var currentUser = await ResolveCurrentUserAsync();
            var entity = await _customerService.Get(request.Id);
            request.ModifiedBy = currentUser.Id;
            request.CreatedBy = entity.CreatedBy;
            return Ok(await _customerService.Put(request));
        }

        [HttpDelete("{customerId}")]
        public async Task<IActionResult> Delete([FromRoute] string customerId)
        {
            var entity = await _customerService.Get(new Guid(customerId));
            return Ok(await _customerService.Delete(entity));
        }

        [HttpGet("{customerId?}")]
        public async Task<IActionResult> Get([FromRoute] string customerId = "")
        {
            return Ok(
                customerId.IsNullOrWhiteSpace() ? await _customerService.Get() : await _customerService.Get(new Guid(customerId)));
        }
    }
}