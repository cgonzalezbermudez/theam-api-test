using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TheAM.Framework.BusinessLayer.DataContext;
using TheAM.Framework.BusinessLayer.Interfaces;
using TheAM.Modules.CustomerData.Entities;

namespace TheAM.Modules.CustomerCore
{
    public class CustomerService : ICustomerService
    {
        private readonly ApiDbContext _dbContext;

        public CustomerService(ApiDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<int> Delete(Customer entity)
        {
            _dbContext.Customers.Remove(entity);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<Customer> Put(Customer entity)
        {
            var response = _dbContext.Customers.Update(entity);
            await _dbContext.SaveChangesAsync();
            return response.Entity;
        }

        public async Task<Customer> Post(Customer entity)
        {
            var response = await _dbContext.Customers.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
            return response.Entity;
        }
        public async Task<Customer> Get(Guid entityId) =>
            await _dbContext.Customers.Where(u => u.Id == entityId).FirstOrDefaultAsync();
        public async Task<List<Customer>> Get() => await _dbContext.Customers.ToListAsync();
    }
}